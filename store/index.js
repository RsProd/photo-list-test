import api from '@/api';

const localStore = localStorage.getItem('favorites');

const sortByName = (a, b) => {
    const nameA = a.title.toLowerCase();
    const nameB = b.title.toLowerCase();

    if (nameA === nameB) {
        return 0;
    }

    return nameA < nameB ? -1 : 1;
};

const groupByName = (photos) => photos.reduce((groups, photo) => {
    const firstLetter = photo.title[0].toLowerCase();
    if (!groups[firstLetter]) {
        groups[firstLetter] = {
            title: firstLetter,
            items: [],
        };
    }
    groups[firstLetter].items.push(photo);

    return groups;
}, {});

const cutPhotos = (albums, photos) => {
    const albumIds = albums.map((el) => el.id);
    const albumsPhotos = [...photos].filter(({ albumId }) => albumIds.includes(albumId));
    let count = 3;

    return albums.map((album) => {
        count = count <= 7 ? count + 1 : 2;

        return albumsPhotos.filter(({ albumId }) => albumId === album.id)
            .slice(0, count);
    })
        .flat()
        .sort(sortByName);
};

export const state = () => ({
    photos: [],
    favorites: localStore ? JSON.parse(localStore) : [],
    albums: [],
});

export const getters = {
    getPhotos: (st) => st.photos,
    getAlbums: (st) => st.albums,
    getFavoritesIds: (st) => st.favorites,
    getFavorites: (st) => Object.keys(st.photos)
        .map((group) => ({
            title: group,
            items: st.photos[group].items.filter(({ id }) => st.favorites.includes(id)),
        }))
        .filter(({ items }) => items && items.length > 0),
};

export const actions = {
    loadPhotos: async (context) => {
        const result = await api.resource('photos');
        const photos = cutPhotos(context.state.albums, result);

        context.commit('fillAlbums', photos);
        context.commit('photosSuccess', photos);
    },

    loadAlbums: async ({ commit }) => {
        const albums = await api.resource('albums', 32);
        commit('albumsSuccess', albums.sort(sortByName));
    },

    updateFavorites: (context, id) => {
        if (context.state.favorites.includes(id)) {
            context.commit('removeFromFavorites', id);
        } else {
            context.commit('addToFavorites', id);
        }

        context.dispatch('updateLocalStore');
    },
    updateLocalStore: (context) => {
        localStorage.setItem('favorites', JSON.stringify(context.state.favorites));
    },
};

export const mutations = {
    albumsSuccess: (st, albums) => {
        st.albums = albums;
    },
    fillAlbums: (st, photos) => {
        st.albums = st.albums.map((album) => ({
            ...album,
            items: photos.filter(({ albumId }) => albumId === album.id),
        }));
    },
    photosSuccess: (st, photos) => {
        st.photos = groupByName(photos);
    },
    removeFromFavorites: (st, id) => {
        st.favorites.splice(st.favorites.indexOf(id), 1);
    },
    addToFavorites: (st, id) => {
        st.favorites.push(id);
    },
};
