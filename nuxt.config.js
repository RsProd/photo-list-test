export default {
    mode: 'spa',

    head: {
        title: 'Evoplay - Photo list test',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
        ],
    },

    router: {
        base: process.env.NODE_ENV === 'production'
            ? '/photo-list-test'
            : '/',
    },

    plugins: [],

    buildModules: [],
};
