import axios from 'axios';

const client = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
});

client.interceptors.response.use(
    (response) => response.data,
    (e) => Promise.reject(e),
);

export default {
    resource (endpoint, _limit) {
        return client.get(endpoint, {
            params: {
                _limit,
            },
        });
    },
};
